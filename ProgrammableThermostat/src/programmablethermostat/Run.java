/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programmablethermostat;

/**
 *
 * @author Rebekah Britton
 */
class Run {
        
    double temp, humid;

    Run(double a, double b) {
        temp = a;
        humid = b;
    }

    public double displayTemp() {
        return temp;
    }

    public double displayHumidity() {
        return humid;
    }
}
