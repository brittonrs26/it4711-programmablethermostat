package programmablethermostat;

/**
 *
 * @author Rebekah Britton
 */
class Identity {

    String ident, name, thermT, utcT;

    Identity(String a, String b, String c, String d) {
        ident = a;
        name = b;
        thermT = c;
        utcT = d;
    }

    public String displayIdentity() {
        return ident;
    }

    public String displayName() {
        return name;
    }

    public String displayThermostatTime() {
        return thermT;
    }

    public String displayUTCTime() {
        return utcT;
    }
}     