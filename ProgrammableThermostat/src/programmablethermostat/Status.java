/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programmablethermostat;

/**
 *
 * @author Rebekah Britton
 */
class Status {

    int code;
    String statusMessage;

    Status(int a, String b) {
        code = a;
        statusMessage = b;
    }

    public int displayCode() {
        return code;
    }

    public String displayStatus() {
        if (statusMessage.length() < 1) {
            return "Idle";
        } else {
            return statusMessage;
        }
    }
}
